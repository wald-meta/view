
wald:view
=========

A collection of javascript modules to load Linked Data into a datastore,
query it for information about a particular subject, and render that
information using a few React components built for displaying Linked Data.

usage
-----

wald:view uses jsx, though translated files are provided in dist/.

    var view = require('wald-view');


License
=======

Copyright (C) 2015  Kuno Woudt <kuno@frob.nl>

This program is free software: you can redistribute it and/or modify
it under the terms of copyleft-next 0.3.0.  See [LICENSE.txt](LICENSE.txt).
