/**
 *   This file is part of wald:find.
 *   Copyright (C) 2015  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.
 */

'use strict';

define(['exports', 'n3/browser/n3-browser', 'react', 'underscore', 'underscore.string'], function (exports, N3, React, _, s) {
    var _array = function _array(item) {
        return _(item).isArray() ? item : [item];
    };

    var Image = React.createClass({
        displayName: 'Image',
        propTypes: {
            src: React.PropTypes.oneOfType([React.PropTypes.array, React.PropTypes.string]).isRequired
        },
        render: function render() {
            var src = _array(this.props.src);

            if (!src.length) {
                return null;
            }

            var sizes = _(src).map(function (lnk) {
                var size = lnk.match(/([0-9]+)x([0-9]+)/);

                if (size.length > 2) {
                    return parseInt(size[1], 10) * parseInt(size[2], 10);
                } else {
                    return false;
                }
            });

            var max = 0;
            var iri = src[0];

            for (var i = 0; i < sizes.length; i++) {
                if (sizes[i] > max) {
                    max = sizes[i];
                    iri = src[i];
                }
            }

            if (!iri) {
                return null;
            }

            return React.createElement('img', {
                src: iri,
                style: {
                    maxWidth: 128
                }
            });
        }
    });
    var QName = React.createClass({
        displayName: 'QName',
        propTypes: {
            datastore: React.PropTypes.instanceOf(N3.Store).isRequired,
            iri: React.PropTypes.string.isRequired
        },
        render: function render() {
            var s_iri = s(this.props.iri);
            var qname = this.props.iri;
            var prefixed = false;

            _(this.props.datastore._prefixes).find(function (basePath, prefix) {
                if (s_iri.startsWith(basePath)) {
                    qname = qname.replace(basePath, prefix + ':');
                    prefixed = true;
                    return true;
                }

                return false;
            });

            if (!prefixed) {
                qname = this.props.iri.slice(this.props.iri.lastIndexOf('/') + 1);
            }

            return React.createElement('a', {
                href: this.props.iri
            }, qname);
        }
    });
    var KeyValue = React.createClass({
        displayName: 'KeyValue',
        propTypes: {
            predicate: React.PropTypes.string.isRequired,
            subject: React.PropTypes.object.isRequired
        },
        render: function render() {
            var self = this;
            var predicate = self.props.predicate;
            var values = self.props.subject.list(predicate);
            var prefix = '';
            var label = predicate;
            var parts = predicate.split(':');

            if (parts.length == 2) {
                prefix = parts[0];
                label = parts[1];
            }

            var renderValue = function renderValue(value) {
                var valueStr = '';

                if (value) {
                    if (N3.Util.isIRI(value)) {
                        valueStr = React.createElement(QName, {
                            datastore: self.props.subject.datastore,
                            iri: value
                        });
                    } else {
                        valueStr = N3.Util.getLiteralValue(value);
                    }
                }

                var key = predicate + ' ' + value;
                return React.createElement('span', {
                    key: key
                }, React.createElement('div', {
                    className: 'label-wrapper'
                }, React.createElement('span', {
                    className: 'prefix'
                }, prefix, ':'), React.createElement('span', {
                    className: 'label'
                }, label, ':'), ' '), React.createElement('span', {
                    className: 'value'
                }, valueStr), React.createElement('br', null));
            };

            return React.createElement('span', null, _(values).map(renderValue));
        }
    });
    exports.Image = Image;
    exports.KeyValue = KeyValue;
    exports.QName = QName;
});
