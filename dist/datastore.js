/**
 *   This file is part of wald:find.
 *   Copyright (C) 2015  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.0.  See LICENSE.txt.
 */

'use strict';

define(['exports', './ldf', 'n3/browser/n3-browser', 'httpinvoke/httpinvoke-browser', 'jsonld', 'when'], function (exports, LDF, N3, httpinvoke, jsonld_module, when) {
    function _typeof(obj) {
        return obj && typeof Symbol !== "undefined" && obj.constructor === Symbol ? "symbol" : typeof obj;
    }

    var jsonld = jsonld_module.promises;

    var streamTurtle = function streamTurtle(data) {
        var chunkSize = 1024 * 1024;
        var dataSize = data.length;
        var offset = 0;
        var delay = 50;
        var parser = new N3.Parser();
        var useNprogress = window.NProgress !== undefined;

        if (useNprogress) {
            window.NProgress.start();
        }

        var nextChunk = function nextChunk() {
            if (offset < dataSize) {
                if (useNprogress) {
                    window.NProgress.set(offset / dataSize);
                }

                parser.addChunk(data.slice(offset, chunkSize + offset));
                offset += chunkSize;
                setTimeout(nextChunk, delay);
            } else {
                parser.end();

                if (useNprogress) {
                    window.NProgress.done();
                }
            }
        };

        setTimeout(function () {
            nextChunk();
        }, delay);
        return parser;
    };

    var parseTurtle = function parseTurtle(input, datastore) {
        var deferred = when.defer();
        var parser = streamTurtle(input);
        parser.parse(function (err, triple, prefixes) {
            if (err) {
                deferred.reject(err);
            } else if (triple) {
                datastore.addTriple(triple);
            } else {
                datastore.addPrefixes(prefixes);
                deferred.resolve(datastore);
            }
        });
        return deferred.promise;
    };

    var parseJsonLD = function parseJsonLD(input, datastore) {
        var data = JSON.parse(input);
        var options = {
            format: 'application/nquads'
        };
        return jsonld.toRDF(data, options).then(function (dataset) {
            return parseTurtle(dataset, datastore);
        });
    };

    var loadTurtle = function loadTurtle(iri, datastore) {
        if (!datastore) {
            datastore = new N3.Store();
        }

        return when(httpinvoke(iri, 'GET')).then(function (data) {
            return parseTurtle(data, datastore);
        });
    };

    var loadFragments = function loadFragments(server, subject, datastore) {
        var ldf = new LDF(server, datastore);
        return ldf.query({
            subject: subject
        });
    };

    var loadJsonLD = function loadJsonLD(iri, datastore) {
        if (!datastore) {
            datastore = new N3.Store();
        }

        if (typeof iri === 'string') {
            return when(httpinvoke(iri, 'GET')).then(function (data) {
                return parseJsonLD(data.body, datastore);
            });
        } else if ((typeof iri === 'undefined' ? 'undefined' : _typeof(iri)) === 'object' && iri instanceof HTMLElement) {
            return parseJsonLD(iri.textContent, datastore);
        } else {
            when.error('unsupported iri type in loadJsonLD, expected HTMLElement or string');
        }
    };

    exports.loadFragments = loadFragments;
    exports.loadTurtle = loadTurtle;
    exports.loadJsonLD = loadJsonLD;
});
